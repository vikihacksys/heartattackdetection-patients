const express = require('express');
const router = express.Router();
const mongoose = require('mongoose')
const request = require("request");

const PatientsList = require('../models/patientsListModel')


/* 

	API: "http://localhost:8082/patientsList/"

	Parameters: 
		doctor_mobile_number: +491543116252324

	HTTP-method: POST	

*/

router.post('/', (req, res, next) => {

	PatientsList.find({'doctor_mobile_number': req.body.doctor_mobile_number})
    	.exec()
    	.then(result => {
 
    		if (result.length > 0) { 
 
    			var fetchedData = JSON.parse(JSON.stringify(result))  
    			var arrPatientsList = fetchedData[0].patients_list 

    			console.log("Arr : ", arrPatientsList)
    			request.post({

				    "headers": { "content-type": "application/json" },
				    "url": "http://ec2-18-220-174-229.us-east-2.compute.amazonaws.com:8080/fetchUsers",
				    "body": JSON.stringify({
				        "doctor_mobile_number": req.body.doctor_mobile_number,
				        "patients": arrPatientsList
				    })

				}, (error, response, body) => { 

					console.log("response : ", response)
					console.log("body : ", body)
					let parsedData = JSON.parse(body)
					console.log("parsedData " + parsedData) 	
					
				    if(error) {

				        res.status(500).json({
							status: false,
							message: "",  
							data: error
						})

				    } else {

				    	if (parsedData.status) { 

							res.status(200).json({
								status: true,
								message: "", 
								data: parsedData.data
							})

						} else { 	

							res.status(200).json({
								status: false,
								message: "No patients registered.", 
								data: {} 
							})

						}

				    } 

				}); 

    		} else { 

			    res.status(200).json({ 
					status: false,
					message: "You have no patients registered.",
					data: {} 
	    		})  
					 
    		}
    		
    	})
    	.catch(error => { 
    		sendErrorResponse(res, error)	
    	})

})



/* 

	API: "http://localhost:8082/patientsList/addPatient"

	Parameters: 
		doctor_mobile_number: +491543116252324
		patient_mobile_number: +491543116255553

	HTTP-method: POST	

*/

router.post('/addPatient', (req, res, next) => {

	PatientsList.find({'doctor_mobile_number': req.body.doctor_mobile_number})
    	.exec()
    	.then(result => {
 
    		if (result.length > 0) { 
 
    			var fetchedData = JSON.parse(JSON.stringify(result))  
    			var patientExist = false 
    			var arrPatientsList = fetchedData[0].patients_list
    			var arr = []

    			for (var i = 0; i < arrPatientsList.length; i++) {
    				arr[i] = arrPatientsList[i]
    				if (arrPatientsList[i] === req.body.patient_mobile_number) {
    					patientExist = true
    				} 
    			}  

 				if (patientExist) {
 	
 					res.status(200).json({ 
	    				status: false,
	    				message: "Patient is already registered under you.",
	    				data: {} 
    		    	})

 				} else { 

					arr.push(req.body.patient_mobile_number) 
					PatientsList.findByIdAndUpdate(fetchedData[0]._id, {"patients_list": arr}, {new: true}, function(err, data) {

						if (err) {
							 
							res.status(200).json({ 
			    				status: false,
			    				message: "",
			    				data: err
			    		    })

						} else { 
							res.status(200).json({ 
			    				status: true,
			    				message: "Patient added successfully.",
			    				data: data
			    		    })
						}

					}) 					

 				} 

    		} else {
 
			    let patientsList = new PatientsList ({
					_id: new mongoose.Types.ObjectId(),
				    doctor_mobile_number: req.body.doctor_mobile_number,
				    patients_list: [req.body.patient_mobile_number]
				})  

				patientsList.save()
					.then(result => { 
						    res.status(200).json({ 
		    				status: true,
		    				message: "Patient added successfully.",
		    				data: patientsList
    		    		})  
					})
					.catch(error => { 
						sendErrorResponse(res, error)
					})
    		}
    		
    	})
    	.catch(error => { 
    		sendErrorResponse(res, error)	
    	})
 
})  



/* 

	API: "http://localhost:8082/patientsList/removePatient"

	Parameters:  
		patient_mobile_number: +491543116255553

	HTTP-method: DELETE	

*/

router.post('/removePatient', (req, res, next) => {

	 PatientsList.find({'doctor_mobile_number': req.body.doctor_mobile_number})
    	.exec()
    	.then(result => {
 
    		if (result.length > 0) { 
 
    			var fetchedData = JSON.parse(JSON.stringify(result))  
    			var patientExist = false 
    			var arrPatientsList = fetchedData[0].patients_list
    			var arr = []
 
    			for (var i = 0; i < arrPatientsList.length; i++) {
    				arr[i] = arrPatientsList[i]
    				if (arrPatientsList[i] === req.body.patient_mobile_number) {
    					patientExist = true
    				} 
    			}  

    			var index = arr.indexOf(req.body.patient_mobile_number);
			    if (index > -1) {
			        arr.splice(index, 1);
			    }

 				if (!patientExist) {
 	
 					res.status(200).json({ 
	    				status: false,
	    				message: "No such patient is registered.",
	    				data: {} 
    		    	})

 				} else { 
  
					PatientsList.findByIdAndUpdate(fetchedData[0]._id, {"patients_list": arr}, {new: true}, function(err, data) {

						if (err) {
							 
							res.status(200).json({ 
			    				status: false,
			    				message: "",
			    				data: err
			    		    })

						} else { 
							res.status(200).json({ 
			    				status: true,
			    				message: "Patient removed successfully.",
			    				data: data
			    		    })
						}

					}) 					

 				} 

    		 } else {
 
			    res.status(200).json({ 
    				status: false,
    				message: "No such doctor registered.",
    				data: {} 
	    		})

    		} 

    	})
    	.catch(error => { 
    		sendErrorResponse(res, error)	
    	})

})



/* 

	API: "http://localhost:8082/patientsList/deletePatient"

	Parameters: 
		user_mobile_number: +491543116252324
		user_type: "Doctor"/"Patient"

	HTTP-method: POST	

*/

router.post('/deletePatient', (req, res, next) => {
 
	// console.log("user_type : ", req.body.user_type)

	if (req.body.user_type == "Doctor") {

		PatientsList.remove({ "doctor_mobile_number": req.body.user_mobile_number })
		.exec()
		.then(result => {

			console.log("Result doctor : ", result)
			if (result.ok) {

				res.status(200).json({
					status: true,
					message: "Doctor's data deleted successfully.",
					data: result
			    })

			} else {

				res.status(200).json({
					status: false,
					message: "",
					data: result
			    })

			}

		})
		.catch(error => { 
    		sendErrorResponse(res, error)	
    	})
 
    } else if (req.body.user_type == "Patient") {

    	PatientsList.updateMany({}, { $pull: { patients_list: req.body.user_mobile_number } }, { multi: true }, function(err, data) {

			if (err) {
				
				console.log("Patient err : ", err)
				res.status(200).json({
					status: false,
					message: "",
					data: err
			    })

			} else {

				console.log("Patient data : ", data)
				res.status(200).json({
					status: true,
					message: "Patient's data deleted successfully.",
					data: data
			    })
			}

		})

    } else {

    	console.log("In else part.....")
    	res.status(200).json({
			status: false,
			message: "Invalid user type.",
			data: {}
	    })

    }
 
})


function sendErrorResponse (res, error) {
	res.status(500).json({
		status: false,
		message: "",
		data: error
    })
} 

module.exports = router