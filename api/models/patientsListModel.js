const mongoose = require('mongoose')

const patientsListSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	doctor_mobile_number: { type: String, required: true},
	patients_list: [{ type: String, required: true}]  
})

module.exports = mongoose.model('PatientsList', patientsListSchema)